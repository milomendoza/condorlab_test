# Web Condorlab-test -- Frontend

| STACK       | COMMANDS                           | PORT                   |
| ----------- |:----------------------------------:| ----------------------:|
| React JS    | first run **npm i**                | 808n => n = 1,2 or any |
| SCSS        | then run **npm run start**         |                        |