/**
	Autor: Juan Camilo Mendoza
	Maintainer: Juan Camilo Mendoza
	Last modification: Juan Camilo Mendoza
*/

//REACT
import React from "react";
import { Route, HashRouter } from 'react-router-dom';
import Favicon from 'react-favicon';

//IMGS
import fav from './img/logo.jpg';

//COMPONENTS
import Home  from './home/Home'

//CONTEXT
import {MProvider} from './provider/Provider'

const App = () => {
    return (
        <HashRouter>
            <div className="App">
                <Favicon url={fav}/>
                <MProvider>
                <Home/>
                </MProvider>
            </div>
        </HashRouter>
    )
}
export default App
