/**
	Autor: Juan Camilo Mendoza
	Maintainer: Juan Camilo Mendoza
	Last modification: Juan Camilo Mendoza
*/

//REACT
import React from 'react'

//CONTEXT
import {MContext} from '../provider/Provider'

//STYLE
import style from "./browsecourser.scss";

//COMPONENTS
import Seeker from '../seeker/Seeker'
import Leftfilter from '../leftfilter/Leftfilter'

//IMG
import down_arrow_w from '../img/down-arrow-w.svg'
import Content from '../content/Content';

const Browsecourser  = (props) => {
    
    return(
        <div className="Browsecourser-box">
            <MContext.Consumer>
                {(context) => (
                    <React.Fragment>
                        <div className="Browsecourser-top">
                            <div className="Browsecourser-rowone">
                                Find CE for a 
                                <div className="Button-c">
                                    <p className="Title-button">Florida</p>
                                    <img className="Arrow-icon" src={down_arrow_w} alt="down_arrow_w"/>
                                </div>
                                <div className="Button-c">
                                    <p className="Title-button">Medical Doctor</p>
                                    <img className="Arrow-icon" src={down_arrow_w} alt="down_arrow_w"/>
                                </div>
                            </div>
                            <div className="Browsecourser-rowttwo">
                                <Seeker getCoursesProviders={context.getCoursesProviders}/>
                                <div className="Seeker-options">
                                    {context.state.seekerOptions.map((item, i) => {
                                        return(
                                            <div className={`Option  ${ item.isSelected ? 'isSelected' : '' }`} 
                                            onClick={() => context.setSeekerOption(item.id)} key={i}>{item.name}
                                            </div>
                                        )   
                                    })}
                                </div>
                            </div>
                            <div className="Browsecourser-rowtree">
                            </div>
                        </div>
                        <div className="Browsecourser-center">
                            <Leftfilter/>
                            <Content/>
                        </div>
                    </React.Fragment>
                )}
            </MContext.Consumer>
        </div>
    )
}

export default Browsecourser;