/**
	Autor: Juan Camilo Mendoza
	Maintainer: Juan Camilo Mendoza
	Last modification: Juan Camilo Mendoza
*/

//REACT
import React from 'react'

//CONTEXT
import {MContext} from '../provider/Provider'

//STYLE
import style from "./content.scss";

//COMPONENTS
import Specialcourse from "./../specialcourse/Specialcourse"
import Course from "./../course/Course"

//IMG
import down_arrow_b from '../img/down-arrow-b.svg'

const Content  = (props) => {
    return(
        <div className="Content-box">
            <MContext.Consumer>
                {(context) => (
                    <React.Fragment>
                        <div className="Content-top">
                            <div className="Content-pages">Page {context.state.currentPage} <span className="Of">of</span> {context.state.courses && context.state.courses.totalItems} result
                                <div className="Content-pager">
                                    <div className={`Pager ${context.state.currentPage === 1 ? 'Pager-disable' : ''}`} onClick={()=>context.changePage(0)}>
                                        {context.state.arrow.left}
                                    </div>
                                    -
                                    <div className={`Pager ${context.state.currentPage === context.state.pagesAmount ? 'Pager-disable' : ''}`} onClick={()=>context.changePage(1)}>
                                        {context.state.arrow.right}
                                    </div>
                                </div>
                            </div>
                            <div className="Content-sorted">
                                <label className="Sorted-label">Sorted by:</label> 
                                <div className="Button-a">
                                    <p className="Title-button">Relevance</p>
                                    <img className="Arrow-icon" src={down_arrow_b} alt="down_arrow_b"/>
                                </div>
                            </div>
                        </div>
                        <div className="Content-center">
                            {context.state.firtscourses && context.state.firtscourses.map((item, i) => {
                                return(
                                    <div key={i}>
                                        <Specialcourse info={item}/>
                                    </div>
                                )   
                            })}
                            {context.state.courses && context.state.courses.items.map((item, i) => {
                                return(
                                    <div key={i}>
                                        <Course info={item}/>
                                    </div>
                                )   
                            })}
                        </div>
                    </React.Fragment>
                )}
            </MContext.Consumer>
        </div>
    )
}

export default Content;