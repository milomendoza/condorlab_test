/**
	Autor: Juan Camilo Mendoza
	Maintainer: Juan Camilo Mendoza
	Last modification: Juan Camilo Mendoza
*/

//REACT
import React from 'react'

//CONTEXT
import {MContext} from '../provider/Provider'

//STYLE
import style from "./course.scss";

//COMPONENTS

//IMGS
import share from './../img/share.svg';
import clock from './../img/clock.svg';
import laptop from './../img/laptop.svg';

const Course  = (props) => {
    return(
        <div className="Course-box">
            <div className="Course-left">
                <p className="Course-name">{props.info.course.name}</p>
                <p className="Course-locations">{props.info.course.provider.name}</p>
                <div className="Course-left-info">
                    <div className="Item-info">
                        <img className="Share-icon" src={clock} alt="clock"/>
                        <p className="Item-hours">{props.info.totalHours} Hours</p>
                    </div>
                    <div className="Item-info">
                        <img className="Share-icon" src={laptop} alt="laptop"/>
                        {props.info.course.deliveryMethod.description &&
                            <p className="Item-hours">{props.info.course.deliveryMethod.description}</p>
                        }
                        {!props.info.course.deliveryMethod.description &&
                            <p className="Item-hours"> --- </p>
                        }
                    </div>
                </div>
            </div>
            <div className="Course-right">
                {props.info.hasPrice &&
                    <p className="Course-price">$ {props.info.price}</p>
                }
                {!props.info.hasPrice &&
                    <p className="Course-price">Free</p>
                }
                <div className="Share-box">
                    <img className="Share-icon" src={share} alt="share"/>
                </div>  
            </div>
        </div>
    )
}

export default Course;