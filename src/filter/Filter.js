/**
	Autor: Juan Camilo Mendoza
	Maintainer: Juan Camilo Mendoza
	Last modification: Juan Camilo Mendoza
*/

//REACT
import React from 'react'

//CONTEXT
import {MContext} from '../provider/Provider'

//STYLE
import style from "./filter.scss";

//COMPONENTS

//IMGS
import angle_arrow_down from './../img/angle-arrow-down.svg';
import up_arrow from './../img/up-arrow.svg';

const Filter  = (props) => {
    return(
        <div className="Filter-box">
            <div className="Filter-top" onClick={()=> props.setOpen(props.info.id)}>
                <p className="Filter-title">{props.info.name}</p>
                {props.info.open &&
                    <img className="Filter-icon" src={up_arrow} alt="up_arrow"/>
                }{!props.info.open &&
                    <img className="Filter-icon" src={angle_arrow_down} alt="angle_arrow_down"/>
                }
            </div>
            {props.info.open &&
                <div className="Filter-center">
                    {props.info.list.map((item, i) => {
                        return(
                            <div key={i}>
                                {i<= 3 && !props.info.more &&
                                    <div  className="Filter-item" onClick={()=> props.setFilterState(props.info.id,item.id)}>
                                        {item.isSelected &&
                                            <div className="Ball-on">
                                                <span className="Point-on"/>
                                            </div>
                                        }
                                        {!item.isSelected &&
                                            <div className="Ball-off">
                                            </div>
                                        }
                                        <p className="Filter-name">{item.name}</p>
                                    </div>
                                }
                                {props.info.more &&
                                    <div  className="Filter-item" onClick={()=> props.setFilterState(props.info.id,item.id)}>
                                        {item.isSelected &&
                                            <div className="Ball-on">
                                                <span className="Point-on"/>
                                            </div>
                                        }
                                        {!item.isSelected &&
                                            <div className="Ball-off">
                                            </div>
                                        }
                                        <p className="Filter-name">{item.name}</p>
                                    </div>
                                }
                            </div>
    
                        )
                    })}
                    <div>
                        {props.info.list.length > 4 && !props.info.more &&
                            <p className="View-more" onClick={() => props.setViewMore(props.info.id)}>View more </p>
                        }
                        {props.info.list.length > 4 && props.info.more &&
                            <p className="View-more" onClick={() => props.setViewMore(props.info.id)}>View less </p>
                        }
                    </div>
                </div>
            }
            
        </div>
    )
}

export default Filter;