//REACT
import React from 'react'
import { Link } from "react-router-dom"

//STYLE
import style from "./header.scss";

//IMG
import brandname from '../img/brandname.png';
import down_arrow_b from '../img/down-arrow-b.svg'

const Header = (props) => {
    
    const _header_center = (
        <div className="Header-center">
            {props.info.map((item, i) => {
                return(
                    <div key={i} className="Header-option-wrap">
                        <Link className={`Header-option ${ item.isSelected ? 'isSelected' : '' } `} to={`/${item.route}`}  >
                            {item.name}
                        </Link>
                        {item.id === 2 &&
                            <img className="Arrow-icon" src={down_arrow_b} alt="down_arrow_b"/>
                        }
                    </div>
                )   
            })}
        </div>
    )

    return(
        <div className="Header-box">
            <div className="Header-left">
                <img className="Brand-name" src={brandname} alt="brandname"/>
            </div>
            {_header_center}
            <div className="Header-right">
                <div className="Button-a">
                    Sing in
                </div>  
                <div className="Button-b">
                    7 day trial
                </div>   
            </div>
        </div>
    )
}

export default Header