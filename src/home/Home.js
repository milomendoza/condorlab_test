/**
	Autor: Juan Camilo Mendoza
	Maintainer: Juan Camilo Mendoza
	Last modification: Juan Camilo Mendoza
*/

//REACT
import React from 'react'

//CONTEXT
import {MContext} from '../provider/Provider'
import Header from '../header/Header'
import {Switch, Route} from "react-router-dom";

//STYLE
import style from "./home.scss";

//COMPONENTS
import Browsecourser  from './../browsecourser/Browsecourser'

const Home = (props) => {    
    return (
        <div className="Home-box">
            <MContext.Consumer>
                {(context) => (
                    <React.Fragment>
                        <Header info={context.state.header}/>
                    </React.Fragment>
                )}
            </MContext.Consumer>
            <Switch>
                <Route exact path="/" component={Browsecourser} />
                <Route path="/browsecourser" component={Browsecourser} />
            </Switch>
        </div>
    )
        
}

export default Home;