/**
	Autor: Juan Camilo Mendoza
	Maintainer: Juan Camilo Mendoza
	Last modification: Juan Camilo Mendoza
*/

//REACT
import React from 'react'
import ReactDOM from 'react-dom'
import { BrowserRouter } from "react-router-dom"

//COMPONENTS
import App from "./App";

//STYLE
import style from "./main.scss";

ReactDOM.render(
<BrowserRouter>
    <App />
</BrowserRouter>, document.getElementById("app"))
