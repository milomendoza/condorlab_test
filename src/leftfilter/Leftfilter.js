/**
	Autor: Juan Camilo Mendoza
	Maintainer: Juan Camilo Mendoza
	Last modification: Juan Camilo Mendoza
*/

//REACT
import React from 'react'

//CONTEXT
import {MContext} from '../provider/Provider'

//STYLE
import style from "./leftfilter.scss";

//COMPONENTS
import Filter from '../filter/Filter'


//IMG
import filter from '../img/filter.svg'

const Leftfilter  = (props) => {
    return(
        <div className="Leftfilter-box">
            <MContext.Consumer>
                {(context) => (
                    <React.Fragment>
                        <div className="Leftfilter-top">
                            <img className="Filter-icon" src={filter} alt="filter"/>
                            <p className="Leftfilter-title">filter course result</p>
                        </div>
                        <div className="Leftfilter-center">
                            {context.state.leftFilter.map((item, i) => {
                                return(
                                    <div key={i}>
                                        <Filter info={item} setOpen={context.showFilters} setViewMore={context.showMoreFilters} setFilterState={context.setFilterState}/>
                                    </div>
                                )   
                            })}
                        </div>
                    </React.Fragment>
                )}
            </MContext.Consumer>
        </div>
    )
}

export default Leftfilter;