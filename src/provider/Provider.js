/**
	Autor: Juan Camilo Mendoza
	Maintainer: Juan Camilo Mendoza
	Last modification: Juan Camilo Mendoza
*/

//REACT
import React, { Component } from 'react';
import { withRouter } from 'react-router'

//API
import {ApiPath} from './../env';

//AXIOS
import axios from 'axios';

//CONTEXT
const MyContext = React.createContext()

class Provider extends React.PureComponent{

    //state
    state = {

        //header routes controls
        header:[
            {   id:0, 
                isSelected:false,
                name:'Features', 
                route:'features'
            },
            {
                id:1, 
                isSelected:false,
                name:'Plans', 
                route:'plan'
            },
            {
                id:2, 
                isSelected:false,
                name:'Organizations', 
                route:'organizations'
            },
            {
                id:3, 
                isSelected:true,
                name:'Browse courses', 
                route:'browsecourser'
            },
            {
                id:4, 
                isSelected:false,
                name:'Support', 
                route:'support'
            }
        ],
        
        //seeker options
        seekerOptions:[
            {
                id:1, 
                isSelected:true,
                name:'courses',
            },
            {
                id:2, 
                isSelected:false,
                name:'providers', 
            }
        ],

        //current seeker option
        currentSeekerOption: {
            id:1, 
            isSelected:true,
            name:'courses',
        },
        
        //left filters
        leftFilter:[
            {   
                name:'Course type',
                id:1, 
                open:true,
                more:false,
                list:[
                    {
                        id:1,
                        name:'Self paced',
                        isSelected:true,
                    },
                    {
                        id:2,
                        name:'Live',
                        isSelected:false,
                    }
                ] 
            },
            {   
                name:'Delivery type',
                id:2, 
                open:true,
                more:false,
                list:[
                    {
                        id:1,
                        name:'Any delivery type',
                        isSelected:false,
                    },
                    {
                        id:2,
                        name:'Computer-Based Training',
                        isSelected:false,
                    },
                    {
                        id:3,
                        name:'Correspondence',
                        isSelected:false,
                    },
                    {
                        id:4,
                        name:'Mailed material',
                        isSelected:false,
                    },
                    {
                        id:5,
                        name:'nnnnn nnnnnn',
                        isSelected:false,
                    },
                    {
                        id:6,
                        name:'ppppp ppppp',
                        isSelected:false,
                    }
                ] 
            },
            {   
                name:'Subject area',
                id:3, 
                open:true,
                more:false,
                list:[
                    {
                        id:1,
                        name:'Any subject area',
                        isSelected:false,
                    },
                    {
                        id:2,
                        name:'HIV/AIDS',
                        isSelected:false,
                    },
                    {
                        id:3,
                        name:'End-of-Life Care and Palliative Health Care',
                        isSelected:false,
                    },
                    {
                        id:4,
                        name:'Domestic Violence',
                        isSelected:false,
                    },
                    {
                        id:5,
                        name:'fffff ffff',
                        isSelected:false,
                    },
                    {
                        id:6,
                        name:'rrrr rrrr',
                        isSelected:false,
                    }
                ] 
            },
        ],

        //arrow buttons
        arrow:{
            left:"<",
            right:">"
        },

        //amount of items to bring
        itemAmount:18,

        //current page
        currentPage:1,

        //seeker value
        seekerValue:''
    }

    /**
     * componentWillMount
     */
    componentDidMount(){
        const route = (this.props.history.location.pathname.split('/').pop())
        route ? this.selectHeaderOption(route) : this.selectHeaderOption('browsecourser')
    }

    /**
     * componentDidMount
     */
    componentWillReceiveProps(props, state){
        const route = (this.props.history.location.pathname.split('/').pop())
        route ? this.selectHeaderOption(route) : this.selectHeaderOption('browsecourser')
    }

    /**
     * initCourses
     * initialize two lists: the list of special courses -> firtscourses and the list of normal courses -> courses
     */
    initCourses = () => {
        axios.get(`${ApiPath}/featuredCoursesProfession?profession=36`)
            .then(r => { 
                this.setState({
                    firtscourses:r.data 
                })
            })
            .catch(e => console.error(`error ${e}`))

        axios.get(`${ApiPath}/search/${this.state.currentSeekerOption.name}/?expand=totalItems&pageIndex=${this.state.currentPage}&pageSize=${this.state.itemAmount}&sortField=RELEVANCE&profession=36&courseType=CD_ANYTIME&sortShufflingSeed=27`)
            .then(r => {
                let original = (r.data.totalItems / this.state.itemAmount)
                let calculate = Math.round((r.data.totalItems / this.state.itemAmount))
                this.setState({
                    courses:r.data,
                    pagesAmount: (calculate >= original)  ? calculate : calculate+1,
                    seekerValue:''            
                })
            })
            .catch(e => console.error(`error ${e}`))
    }

    /**
     * changePage
     * change the course page depending on the address you are given
     * {*} id => address ( 1 -> right, 0 -> left)
     */
    changePage = (id) => {
        if(id === 1 ){
            let calculate = this.state.currentPage+1  
            axios.get(`${ApiPath}/search/${this.state.currentSeekerOption.name}/?expand=totalItems&pageIndex=${calculate}&pageSize=${this.state.itemAmount}&sortField=RELEVANCE&profession=36&courseType=CD_ANYTIME&sortShufflingSeed=27&courseName=${this.state.seekerValue}`)
            .then(r => {
                this.setState({
                    courses:r.data,  
                    currentPage:calculate,
                    firtscourses: []              
                })
            })
            .catch(e => console.error(`error ${e}`))
        }else{
            let calculate = this.state.currentPage-1  
            axios.get(`${ApiPath}/search/${this.state.currentSeekerOption.name}/?expand=totalItems&pageIndex=${calculate}&pageSize=${this.state.itemAmount}&sortField=RELEVANCE&profession=36&courseType=CD_ANYTIME&sortShufflingSeed=27&courseName=${this.state.seekerValue}`)
            .then(r => {
                this.setState({
                    courses:r.data,  
                    currentPage:calculate,              
                })
            })
            .catch(e => console.error(`error ${e}`))

            if(calculate === 1 && !this.state.seekerValue){
                axios.get(`${ApiPath}/featuredCoursesProfession?profession=36`)
                    .then(r => { 
                        this.setState({
                            firtscourses:r.data 
                        })
                    })
                    .catch(e => console.error(`error ${e}`))
            }
        }

        
    }

    /**
     * getKid
     * Trae un niño
     * @param {*} e => valor de la busqueda
    */
    getCoursesProviders = (e) => {
        let value = e.target.value
        if(value){
            axios.get(`${ApiPath}/search/${this.state.currentSeekerOption.name}/?expand=totalItems&pageIndex=1&pageSize=${this.state.itemAmount}&sortField=RELEVANCE&profession=36&courseType=CD_ANYTIME&sortShufflingSeed=27&courseName=${value}`)
                .then(r => { 
                    let original = (r.data.totalItems / this.state.itemAmount)
                    let calculate = Math.round((r.data.totalItems / this.state.itemAmount))
                    this.setState({
                        courses:r.data,
                        pagesAmount:(calculate >= original)  ? calculate : calculate+1,
                        firtscourses: [],
                        currentPage:1,
                        seekerValue:value
                    })
                })
                .catch(e => console.error(`error ${e}`))
        }
        else if(!value && e.key === 'Backspace'){
           this.initCourses()
        }
    }

    /**
     * selectHeaderOption
     * change the header option depending on the id received as parameter
     * @param {*} id => option id
    */
    selectHeaderOption=(route)=>{
        this.setState({
            header:[
                 {  id:0, 
                    isSelected:route === 'features' ? true : false,
                    name:'Features', 
                    route:'features'
                },
                {
                    id:1, 
                    isSelected:route === 'plan' ? true : false,
                    name:'Plans', 
                    route:'plan'
                },
                {
                    id:2, 
                    isSelected:route === 'organizations' ? true : false,
                    name:'Organizations', 
                    route:'organizations'
                },
                {
                    id:3, 
                    isSelected:route === 'browsecourser' ? true : false,
                    name:'Browse courses', 
                    route:'browsecourser'
                },
                {
                    id:4, 
                    isSelected:route === 'support' ? true : false,
                    name:'Support', 
                    route:'support'
                }
            ]
        })
        if(route === 'browsecourser'){
            this.initCourses()
        }
    }

    /**
     * setSeekerOption
     * change the seeker option depending on the id received as parameter
     * @param {*} id => option id
    */
    setSeekerOption=(_id)=>{
        this.setState({
            seekerOptions:[
                {
                    id:1, 
                    isSelected:_id === 1 ? true : false,
                    name:'courses', 
                },
                {
                    id:2, 
                    isSelected:_id === 2 ? true : false,
                    name:'providers', 
                }
            ],
        })
    }

    /**
     * showFilters
     * change the state of open param of a filter category
     * @param {*} id => filter category id
    */
    showFilters=(id)=>{
        const array = this.state.leftFilter.map((item ) => {
            if(item.id === id){
                item.open ? item.open = false : item.open = true
            }
            return item  
        })
        this.setState({
            leftFilter:array,
        })
    }

    /**
     * showMoreFilters
     * change the state of more param of a filter category
     * @param {*} id => filter category id
    */
    showMoreFilters=(id)=>{
        const array = this.state.leftFilter.map((item ) => {
            if(item.id === id){
                item.more ? item.more = false : item.more = true
            }
            return item  
        })
        this.setState({
            leftFilter:array,
        })
    }

    /**
     * setFilterState
     * change the state of open param of a filter
     * @param {*} id => filter id
    */
    setFilterState=(id_one, id_two)=>{
        const array = this.state.leftFilter.map((item ) => {
            if(item.id === id_one){
                item.list.forEach(element => {
                    if(element.id === id_two){
                        element.isSelected ? element.isSelected = false : element.isSelected = true
                    }
                });
            }
            return item  
        })
        this.setState({
            leftFilter:array,
        })
    }
    /**
     * 
     */
    render(){
        return(
            <MyContext.Provider value={{state: this.state,
                                        getCoursesProviders: this.getCoursesProviders,
                                        selectHeaderOption: this.selectHeaderOption,
                                        setSeekerOption: this.setSeekerOption,
                                        showFilters:this.showFilters,
                                        showMoreFilters:this.showMoreFilters,
                                        setFilterState:this.setFilterState,
                                        changePage:this.changePage
                                        }} >
              {this.props.children}
            </MyContext.Provider>
        )
    }
    
}

export const MProvider = withRouter(Provider)
export const  MContext = MyContext