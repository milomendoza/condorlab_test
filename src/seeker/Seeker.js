/**
	Autor: Juan Camilo Mendoza
	Maintainer: Juan Camilo Mendoza
	Last modification: Juan Camilo Mendoza
*/

//REACT
import React from 'react'

//STYLE
import style from "./seeker.scss";

//CONTEXT
import {MContext} from '../provider/Provider'

//IMGS
import seeker from './../img/search.svg';

const Seeker  = (props) => {

    return(
        <MContext.Consumer>
            {(context) => (
                <React.Fragment>
                    <div className="Seeker-box">
                        <div className="Seeker-input-wrap">
                            <img className="Seeker-icon" src={seeker} alt="seeker"/>
                            <input id="input-seeker" type="text" onKeyUp={props.getCoursesProviders} className="Seeker-input" placeholder="Search courses and providers"/>
                        </div>
                    </div>
                </React.Fragment>
            )}
        </MContext.Consumer>
    )
}

export default Seeker