/**
	Autor: Juan Camilo Mendoza
	Maintainer: Juan Camilo Mendoza
	Last modification: Juan Camilo Mendoza
*/

//REACT
import React from 'react'

//CONTEXT
import {MContext} from '../provider/Provider'

//STYLE
import style from "./specialcourse.scss";

//COMPONENTS

//IMGS
import share from './../img/share.svg';
import clock from './../img/clock.svg';
import laptop from './../img/laptop.svg';

const Specialcourse  = (props) => {
    return(
        <div className="Specialcourse-box">
            <div className="Specialcourse-left">
                <div className="Logo-course" style={{backgroundImage:`url(https://storage.cebroker.com/CEBroker/${props.info.coursePublication.course.featuredBanner})`}}/>
            </div>
            <div className="Specialcourse-center">
                <p className="Specialcourse-name">{props.info.coursePublication.course.name}</p>
                <div className="Specialcourse-featured">
                    Featured
                </div>
                <p className="Specialcourse-locations">{props.info.coursePublication.course.provider.name}</p>
                <div className="Specialcourse-center-info">
                    <div className="Item-info">
                        <img className="Share-icon" src={clock} alt="clock"/>
                        <p className="Item-hours">{props.info.coursePublication.totalHours} Hours</p>
                    </div>
                    <div className="Item-info">
                        <img className="Share-icon" src={laptop} alt="laptop"/>
                        {props.info.coursePublication.course.deliveryMethod &&
                            <p className="Item-hours">{props.info.coursePublication.course.deliveryMethod}</p>
                        }
                        {!props.info.coursePublication.course.deliveryMethod &&
                            <p className="Item-hours"> --- </p>
                        }
                    </div>
                </div>
            </div>
            <div className="Specialcourse-right">
                {props.info.coursePublication.course.price &&
                    <p className="Specialcourse-price">$ {props.info.coursePublication.course.price}</p>
                }
                {!props.info.coursePublication.course.price &&
                    <p className="Specialcourse-price">Free</p>
                }
                <div className="Share-box">
                    <img className="Share-icon" src={share} alt="share"/>
                </div>  
            </div>
        </div>
    )
}

export default Specialcourse;